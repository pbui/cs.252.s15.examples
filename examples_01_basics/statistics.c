/* statistics.c */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#define NITEMS	1024

int
compute_sum(int *data, size_t n)
{
    int total = 0;

    for (size_t i = 0; i < n; i++) {
    	total += data[i];
    }

    return total;
}

int
compute_min(int *data, size_t n)
{
    int min = INT_MAX;

    for (size_t i = 0; i < n; i++) {
    	if (data[i] < min) {
    	    min = data[i];
	}
    }

    return min;
}

int
compute_max(int *data, size_t n)
{
    int max = INT_MIN;

    for (size_t i = 0; i < n; i++) {
    	if (data[i] > max) {
    	    max = data[i];
	}
    }

    return max;
}

int
main(int argc, char *argv[])
{
    int    data[NITEMS];
    int	   element;
    int	   sum;
    size_t length;

    for (length = 0; scanf("%d", &element) != EOF; length++) {
    	data[length] = element;
    }

    sum = compute_sum(data, length);

    printf("Sum:  %d\n" , sum);
    printf("Mean: %lf\n", (float)(sum) / length);
    printf("Min:  %d\n" , compute_min(data, length));
    printf("Max:  %d\n" , compute_max(data, length));

    return EXIT_SUCCESS;
}
