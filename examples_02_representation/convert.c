/* convert.c: convert integers to different bases */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define INT_BITS (sizeof(int)*8)

const char *
int_to_binary_string(int n)
{
    static char buffer[INT_BITS + 1];

    for (int i = 0; i < INT_BITS; i++) {
    	buffer[INT_BITS - i - 1] = (n & (1<<i)) ? '1' : '0';
    }

    buffer[INT_BITS] = '\0';
    return buffer;
}

int
parse_command_line_options(int argc, char *argv[])
{
    int base = -1;
    int c;

    while ((c = getopt(argc, argv, "bdho")) != -1) {
	switch (c) {
	    case 'b':
		base = 2;
		break;
	    case 'd':
		base = 10;
		break;
	    case 'h':
		base = 16;
		break;
	    case 'o':
		base = 8;
		break;
	    default:
	    	goto failure;
	}
    }

    if (base < 0 || (argc - optind) < 1) {
    	goto failure;
    }

    return base;

failure:
    fprintf(stderr, "usage: %s [-bdho] number\n", argv[0]);
    fprintf(stderr, "   -b Input is binary\n");
    fprintf(stderr, "   -d Input is decimal\n");
    fprintf(stderr, "   -h Input is hexadecimal\n");
    fprintf(stderr, "   -o Input is octal\n");

    exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[])
{
    int base = parse_command_line_options(argc, argv);

    for (int i = optind; i < argc; i++) {
	int n = strtol(argv[i], NULL, base);

	printf("BINARY:      %s\n", int_to_binary_string(n));
	printf("OCTAL:       %o\n", n);
	printf("DECIMAL:     %d\n", n);
	printf("HEXADECIMAL: %x\n", n);
    }

    return EXIT_SUCCESS;
}
