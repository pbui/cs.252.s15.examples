#include "debug.h"

#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_POINTS  1024

struct point_t {
    double x;
    double y;
};

size_t
read_points(struct point_t p[], size_t limit)
{
    size_t i = 0;

    while (i < limit && scanf("%lf %lf", &p[i].x, &p[i].y) == 2) {
    	debug("READ POINT: %lf %lf", p[i].x, p[i].y);
    	i++;
    }

    return i;
}

double
distance(struct point_t p0, struct point_t p1)
{
    double dx = p0.x - p1.x;
    double dy = p0.y - p1.y;

    return sqrt(dx*dx + dy*dy);
}

struct point_t
find_most_distant_point(struct point_t origin, struct point_t p[], size_t length)
{
    struct point_t max;
    double max_distance = LONG_MIN;

    for (size_t i = 0; i < length; i++) {
    	double d = distance(origin, p[i]);
    	if (d > max_distance) {
    	    max = p[i];
    	    max_distance = d;
	}
    }

    return max;
}

int
main(int argc, char *argv[])
{
    struct point_t points[MAX_POINTS];
    struct point_t origin = {0, 0};
    struct point_t result;
    size_t length;

    length = read_points(points, MAX_POINTS);
    result = find_most_distant_point(origin, points, length);

    printf("Most distance point is: (%.2lf, %.2lf) [%.2lf]\n", result.x, result.y, distance(origin, result));

    return EXIT_SUCCESS;
}
