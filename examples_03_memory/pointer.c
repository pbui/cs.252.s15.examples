#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[]) 
{
    int  x = 1;
    int  y = 2;
    int *p = NULL;

    printf("x = %d (%lx)\ny = %d (%lx)\np = %lx (%lx)\n\n", x, (long)&x, y, (long)&y, (long)p, (long)&p);

    puts("p = &x");
    p = &x;
    printf("x = %d (%lx)\ny = %d (%lx)\np = %lx (%lx)\n", x, (long)&x, y, (long)&y, (long)p, (long)&p);
    printf("*p == x ? %d\n", *p == x);
    printf("*p == y ? %d\n\n", *p == y);
    
    puts("p--");
    p--;
    printf("x = %d (%lx)\ny = %d (%lx)\np = %lx (%lx)\n", x, (long)&x, y, (long)&y, (long)p, (long)&p);
    printf("*p == x ? %d\n", *p == x);
    printf("*p == y ? %d\n\n", *p == y);
    
    puts("p++");
    p++;
    printf("x = %d (%lx)\ny = %d (%lx)\np = %lx (%lx)\n", x, (long)&x, y, (long)&y, (long)p, (long)&p);
    printf("*p == x ? %d\n", *p == x);
    printf("*p == y ? %d\n\n", *p == y);
    
    puts("p = &y");
    p = &y;
    printf("x = %d (%lx)\ny = %d (%lx)\np = %lx (%lx)\n", x, (long)&x, y, (long)&y, (long)p, (long)&p);
    printf("*p == x ? %d\n", *p == x);
    printf("*p == y ? %d\n\n", *p == y);

    return EXIT_SUCCESS;
}
