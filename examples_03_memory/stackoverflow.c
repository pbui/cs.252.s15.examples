#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

int
factorial(int x)
{
    if (x == 1) {
    	return 1;
    }

    return x * factorial(x - 1);
}

int
main(int argc, char *argv[])
{
    factorial(1024 * 1024);

    return EXIT_SUCCESS;
}
