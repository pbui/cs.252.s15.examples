#include "vector.h"

#include <assert.h>
#include <stdio.h>

struct vector_t *
vector_create(size_t size, size_t capacity)
{
    struct vector_t *v;

    v = malloc(sizeof(struct vector_t));
    if (v == NULL) {
    	goto failure;
    }

    v->size     = size;
    v->capacity = capacity ? capacity : DEFAULT_CAPACITY;
    v->data	= calloc(capacity, sizeof(int));

    if (v->data == NULL) {
    	goto failure;
    }

    return v;

failure:
    free(v);
    return NULL;
}

void
vector_delete(struct vector_t *v)
{
    assert(v != NULL);

    free(v->data);
    free(v);
}

int
vector_get(struct vector_t *v, size_t index)
{
    assert(v != NULL);
    assert(index < v->size);

    return v->data[index];
}

void
vector_set(struct vector_t *v, size_t index, int value)
{
    assert(v != NULL);
    assert(index < v->size);
    
    v->data[index] = value;
}

void
vector_append(struct vector_t *v, int value)
{
    assert(v != NULL);

    if (v->size == v->capacity) {
    	v->capacity = 8*v->capacity / 5;
    	v->data     = realloc(v->data, sizeof(int)*v->capacity);
    	if (v->data == NULL) {
    	    abort();
	}
    }

    v->data[v->size] = value;
    v->size++;
}

void
vector_print(struct vector_t *v)
{
    assert(v != NULL);

    printf("size:     %lu\n", v->size);
    printf("capacity: %lu\n", v->capacity);

    for (size_t i = 0; i < v->size; i++) {
    	printf("%lu. %d\n", i, v->data[i]);
    }
}
