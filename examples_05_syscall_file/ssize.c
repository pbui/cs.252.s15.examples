/* syscall.c: print file size */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
    size_t ntotal;
    size_t nread;
    int    rfd;
    char   buffer[BUFSIZ];

    for (int i = 1; i < argc; i++) {
    	ntotal = 0;
	rfd    = open(argv[i], O_RDONLY);
	if (rfd < 0) {
	    fprintf(stderr, "cannot open %s: %s\n", argv[i], strerror(errno));
	    exit(EXIT_FAILURE);
	}

	while ((nread = read(rfd, buffer, BUFSIZ)) > 0) {
	    ntotal += nread;
	}
    	
    	close(rfd);

	printf("%s %lu\n", argv[i], ntotal);
    }

    return (EXIT_SUCCESS);
}
