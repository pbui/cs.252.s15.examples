#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

int
main(int argc, char *argv[])
{
    int   pfds[2];
    int   result;
    pid_t pid;
    FILE *fs;
    char  buffer[BUFSIZ];

    /* Create pipe */
    result = pipe(pfds);
    if (result < 0) {
    	perror("pipe");
    	return (EXIT_FAILURE);
    }

    /* Fork */
    pid = fork();
    switch (pid) {
    	/* Error */
	case -1:
	    perror("fork");
	    return (EXIT_FAILURE);
	
	/* Child */
	case 0:
	    /* Open read end of pipe */
	    fs = fdopen(pfds[0], "r");
	    if (fs == NULL) {
		perror("fdopen");
	    	_exit(EXIT_FAILURE);
	    }

	    /* Read and echo pipe */
	    while (fgets(buffer, BUFSIZ, fs)) {
	    	printf("%s:%d %s", argv[0], getpid(), buffer);
	    }

	    break;

	/* Parent */
	default:
	    /* Open write end of pipe */
	    fs = fdopen(pfds[1], "w");
	    if (fs == NULL) {
		perror("fdopen");
	    	_exit(EXIT_FAILURE);
	    }

	    for (int i = 0; i < 10; i++) {
	    	fprintf(fs, "%s:%d HELLO!\n", argv[0], getpid());
	    	fflush(fs);
	    	sleep(1);
	    }
	    break;
    }

    return (EXIT_SUCCESS);
}
