/* stl.cpp: demonstrate STL */

#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

#define MAX_ITEMS 8

void
print_vector(vector<int> &v)
{
    for (size_t i = 0; i < v.size(); i++) {
    	cout << i << ". " << v[i] << endl;
    }
}

int
main(int argc, char *argv[])
{
    vector<int> data;

    /* Seed random number generator */
    srand(time(NULL));

    /* Initialize data vector with random number */
    for (int i = 0; i < MAX_ITEMS; i++) {
    	data.push_back(rand() % MAX_ITEMS);
    }

    print_vector(data);
    
    /* Sort vector */
    sort(data.begin(), data.end());

    puts("Sorted!");
    print_vector(data);
    
    /* Search vector */
    for (int i = 0; i < MAX_ITEMS; i++) {
    	printf("Is %d in data? %d\n", i, binary_search(data.begin(), data.end(), i));
    }
}
