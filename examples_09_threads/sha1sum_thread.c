#include <openssl/sha.h>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dirent.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
    	
void *
sha1sum_thread(void *arg)
{
    char    *path = (char *)arg;
    int      fd;
    int      nread;
    char     buffer[BUFSIZ];
    char     chksum[BUFSIZ];
    uint8_t  digest[SHA_DIGEST_LENGTH];
    SHA_CTX  ctx;

    /* Open file for reading */
    fd = open(path, O_RDONLY);
    if (fd < 0) {
	perror("open");
	goto exit;
    }

    /* Read file and compute checksum */
    SHA1_Init(&ctx);
    while ((nread = read(fd, buffer, BUFSIZ)) > 0) {
	SHA1_Update(&ctx, buffer, nread);
    }
    SHA1_Final(digest, &ctx);

    /* Print checksum */
    chksum[0] = 0;
    buffer[0] = 0;
    for (int i = 0; i < SHA_DIGEST_LENGTH; i++) {
    	if (i % 2) {
	    snprintf(chksum, BUFSIZ, "%s%02x", buffer, digest[i]);
	} else {
	    snprintf(buffer, BUFSIZ, "%s%02x", chksum, digest[i]);
	}
    }
    printf("%s  %s\n", chksum, path);

    /* Close file */
    close(fd);

exit:
    pthread_exit(NULL);
}

int
main(int argc, char *argv[])
{
    pthread_t threads[argc - 1];

    /* Ignore children */
    signal(SIGCHLD, SIG_IGN);

    /* Scan directory, filtering out directories */
    for (int i = 1; i < argc; i++) {
    	int rc = pthread_create(&threads[i], NULL, sha1sum_thread, argv[i]);
	if (rc < 0) {
	    fprintf(stderr, "pthread_create failed: %d\n", rc);
	    return EXIT_FAILURE;
	}
	pthread_detach(threads[i]);
    }

    pthread_exit(NULL);
}
