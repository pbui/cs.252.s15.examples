/* socket_chat_server.cpp: simple TCP chat server (thread) */

extern "C" {
#include "socket.h"
}

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <map>
#include <queue>
#include <set>
#include <string>

using namespace std;

pthread_rwlock_t		ReadWriteLock = PTHREAD_RWLOCK_INITIALIZER;
map<string, queue<string>>	Mailboxes;
map<string, pthread_mutex_t*>	Lockboxes;

struct echo_client {
    string id;
    int    fd;
    FILE  *file;
    char   host[NI_MAXHOST];
    char   port[NI_MAXSERV];
};

int   accept_client(int server_fd, struct echo_client *client);
void* handle_client(void *arg);
void  register_client(struct echo_client *client);
void  unregister_client(struct echo_client *client);

int
main(int argc, char *argv[])
{
    char *port;
    int   server_fd;

    /* Parse command line options */
    if (argc != 2) {
	fprintf(stderr, "usage: %s port\n", argv[0]);
	return EXIT_FAILURE;
    }

    port = argv[1];

    /* Open server socket */
    server_fd = socket_listen(port);
    if (server_fd < 0) {
    	fprintf(stderr, "Unable to open socket on port %s: %s\n", port, strerror(errno));
    	return EXIT_FAILURE;
    }

    /* Accept and handle incoming connections */
    while (true) {
    	struct echo_client *client;
    	pthread_t thread;

	/* Allocate client struct */
	client = new struct echo_client;

	/* Accept client */
    	if (accept_client(server_fd, client) < 0) {
    	    fprintf(stderr, "Could not accept client: %s\n", strerror(errno));
    	    continue;
	}

	/* Register client */
	register_client(client);

	/* Launch and detach thread */
	pthread_create(&thread, NULL, handle_client, client);
	pthread_detach(thread);
    }

    return EXIT_SUCCESS;
}

int
accept_client(int server_fd, struct echo_client *client)
{
    struct sockaddr client_addr;
    socklen_t       client_size;

    /* Accept a client */
    client_size = sizeof(client_addr);
    client->fd  = accept(server_fd, &client_addr, &client_size);
    if (client->fd < 0) {
    	return -1;
    }

    /* Lookup client information */
    if (getnameinfo(&client_addr, client_size, client->host, NI_MAXHOST, client->port, NI_MAXSERV, NI_NUMERICHOST | NI_NUMERICSERV) != 0) {
    	close(client->fd);
    	return -1;
    }

    client->id = client->host + string(":") + client->port;

    /* Open socket stream */
    client->file = fdopen(client->fd, "r+");
    if (client->file < 0) {
    	close(client->fd);
    	return -1;
    }

    return 0;
}

void *
handle_client(void *arg)
{
    struct echo_client *client = (struct echo_client *)arg;
    char buffer[BUFSIZ];

    fprintf(stderr, "%s:%s connected\n", client->host, client->port);

    /* Handle messages from client */ 
    while (true) {
	if (fgets(buffer, BUFSIZ, client->file) == NULL) {
	    break;
	}

	/* SEND operation 
	 *
	 * This takes the message sent by the client and adds it to the queue
	 * of every other client for delivery.
	 */
	if (strncmp(buffer, "SEND ", 5) == 0) {
	    fprintf(stderr, "%s:%s SEND START\n", client->host, client->port);
	    fprintf(stderr, "%s:%s %s\n", client->host, client->port, buffer);

	    string message = buffer + 5;
	    pthread_rwlock_rdlock(&ReadWriteLock); {
	    	for (auto it = Mailboxes.begin(); it != Mailboxes.end(); it++) {
	    	    pthread_mutex_lock(Lockboxes[it->first]); {
	    	    	if (it->first != client->id) {
			    it->second.push(message);
			}
		    } pthread_mutex_unlock(Lockboxes[it->first]);
		}
	    } pthread_rwlock_unlock(&ReadWriteLock);
	    fprintf(stderr, "%s:%s SEND END\n", client->host, client->port);
	/* RECV operation 
	 *
	 * This fetches the messages from a client and sends the messages to the client. 
	 *
	 */
	} else if (strncmp(buffer, "RECV", 4) == 0) {
	    fprintf(stderr, "%s:%s RECV START\n", client->host, client->port);
	    pthread_mutex_lock(Lockboxes[client->id]); {
	    	queue<string> &mailbox = Mailboxes[client->id];
	    	while (!mailbox.empty()) {
		    string message = mailbox.front();
	    	    fputs(message.c_str(), client->file);
		    mailbox.pop();
		}
	    } pthread_mutex_unlock(Lockboxes[client->id]);
	    fprintf(stderr, "%s:%s RECV END\n", client->host, client->port);
	} else {
	    fprintf(stderr, "%s:%s invalid request: %s\n", client->host, client->port, buffer);
	}
    }

    fprintf(stderr, "%s:%s disconnected\n", client->host, client->port);

    /* Close socket, unregister client, free client, exit thread */
    close(client->fd);
    unregister_client(client);
    free(client);
    pthread_exit(NULL);
}

void
register_client(struct echo_client *client)
{
    /* Initialize client queue and lock */
    pthread_rwlock_wrlock(&ReadWriteLock); {
	Mailboxes[client->id] = queue<string>();
	Lockboxes[client->id] = new pthread_mutex_t;
	pthread_mutex_init(Lockboxes[client->id], NULL);
    } pthread_rwlock_unlock(&ReadWriteLock);
}

void
unregister_client(struct echo_client *client)
{
    /* Destroy client mutex, remove client lock, remove client queue */
    pthread_rwlock_wrlock(&ReadWriteLock); {
	pthread_mutex_destroy(Lockboxes[client->id]);
	Lockboxes.erase(client->id);
	Mailboxes.erase(client->id);
    } pthread_rwlock_unlock(&ReadWriteLock);
}
